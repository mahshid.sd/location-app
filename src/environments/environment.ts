// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  API_BASE_URL: 'http://localhost:3000',
  mapBoxKey: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjU3N2VhN2RhZTY4NzBjZjNmNmNhNmEzMTE1YWU5ODkyMTZlYzhhMjMzOTU3OWZkY2M3YjRiNTc2MzNmNzVkMDI3OWM5NzBiMmIwMWQxYjhhIn0.eyJhdWQiOiIxMDU3MiIsImp0aSI6IjU3N2VhN2RhZTY4NzBjZjNmNmNhNmEzMTE1YWU5ODkyMTZlYzhhMjMzOTU3OWZkY2M3YjRiNTc2MzNmNzVkMDI3OWM5NzBiMmIwMWQxYjhhIiwiaWF0IjoxNTk4MTY0NzEzLCJuYmYiOjE1OTgxNjQ3MTMsImV4cCI6MTYwMDg0NjcxMywic3ViIjoiIiwic2NvcGVzIjpbImJhc2ljIl19.m_BgwN_YSvcKWdMAhIHIEw2YUMl9Whg4uGpzULOjcCu7ImCXHRDen1Xa8zdo2MuuX8MUVfURb7WgnwAZqr9cAIETBia4szJQ9NUiNTOtP0dqJpxnOAHYiObKR1msHRuO7dAKaTsg89SpHbOuJZ3TOY0AfkOM_e4A5ekUW5bVtSpn744yie2M0yNm2UUg8AqwSYCuBGQFICLpgQVTR5nBpbjNXYTlKgMRzjOaGOjCZxwhzOBTnlsU7ZG1aYRXoFZ_52u46MbT7LXIt-B-k4xTc07XoU3CQQ_UNr61qNIQrjL5xEPRS1gvgR9RMfqf4-_YOLOBRqEW3wFoLGdbn7xUmQ'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
