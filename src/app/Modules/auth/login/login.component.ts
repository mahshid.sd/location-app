import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from 'src/app/Services/auth.service';
import { UserService } from '../../../Services/user.service';

import * as jwt_decode from "jwt-decode";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  accessToken: any;
  user: any;

  constructor(public authService: AuthService, public router: Router, public userService: UserService) { }

  ngOnInit(): void {
  }

  userForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
  });

  loginUser = {}

  getUserByJWT(data: any){
    this.accessToken = data['accessToken'];
    this.user = jwt_decode(this.accessToken);
    localStorage.setItem('accessToken', JSON.stringify(this.user));
  }

  getUserInfo(id: number) {
    return this.userService.getUser(id);
  }

  onLogin() {
    this.loginUser =  this.loginUserInfo();
    this.authService.login(this.loginUser).subscribe(data => {
        this.getUserByJWT(data)
        this.router.navigate(['/favorite'])
    },
      error => console.log(error))
  }

  loginUserInfo() {
    return {"email": this.userForm.value.email, "password": this.userForm.value.password }
  }

}
