import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { AuthService } from 'src/app/Services/auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  constructor(public authService: AuthService) { }

  ngOnInit(): void {
  }

  user = {}
  userForm = new FormGroup({
    name: new FormControl(''),
    password: new FormControl(''),
    phoneNumber: new FormControl(''),
    email: new FormControl('')
  });

  onSignUp() {
    this.user = this.getUserForm()
    this.authService.SignUp(this.user).subscribe();
    this.userForm.reset()
  }

  getUserForm() {
    return { "email": this.userForm.value.email, "password": this.userForm.value.password, "name": this.userForm.value.name, "phoneNumber": this.userForm.value.phoneNumber}
  }
}
