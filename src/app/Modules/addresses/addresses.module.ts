import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { AddressCardComponent } from './address-card/address-card.component';
import { PublicAddressComponent } from './public-address/public-address.component';
import { FavoriteAddressComponent } from './favorite-address/favorite-address.component';
import { AddAddressComponent } from './add-address/add-address.component';

import { addressRoutingModule } from './address-routing.module';

import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';


import { AuthGuard } from './Guard/auth.guard';
import { MapComponent } from './map/map.component';


@NgModule({
  declarations: [AddressCardComponent, PublicAddressComponent, FavoriteAddressComponent, AddAddressComponent, MapComponent],
  imports: [
    CommonModule,
    addressRoutingModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    MatButtonModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSnackBarModule
  ],
  providers: [AuthGuard],
})
export class AddressesModule { }
