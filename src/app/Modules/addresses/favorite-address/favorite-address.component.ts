import { Component, OnInit } from '@angular/core';

import { FavoriteAddressService } from 'src/app/Services/favorite-address.service';
import { AuthService } from '../../../Services/auth.service';

@Component({
  selector: 'app-favorite-address',
  templateUrl: './favorite-address.component.html',
  styleUrls: ['./favorite-address.component.scss']
})
export class FavoriteAddressComponent implements OnInit {

  constructor(public favoriteAddress: FavoriteAddressService, public authService: AuthService) { }

  ngOnInit(): void {
    this.getaddress()
  }

  addresses: any;
  userId: number;

  getaddress() {
    this.userId = JSON.parse(localStorage.getItem('accessToken')).sub
    this.favoriteAddress.getFavoriteAddress(this.userId).subscribe(data => {
      this.addresses = data;
    })
  }

}
