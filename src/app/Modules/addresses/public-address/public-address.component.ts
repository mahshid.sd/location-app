import { Component, OnInit } from '@angular/core';

import { AddressService } from 'src/app/Services/address.service';

@Component({
  selector: 'app-public-address',
  templateUrl: './public-address.component.html',
  styleUrls: ['./public-address.component.scss']
})
export class PublicAddressComponent implements OnInit {

  constructor(public addressService: AddressService) { }

  ngOnInit(): void {
    this.getaddress()
  }

  addresses: any;

  getaddress() {
    this.addressService.getAddress().subscribe((data: any) => {
      this.addresses = data;
    });
  }
}
