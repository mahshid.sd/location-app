import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';

import { environment } from 'src/environments/environment';
import * as mapboxgl from 'mapbox-gl';

declare var Mapp;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  @Output() lngLatEvent = new EventEmitter<any>();
  @Input() coordinates: any
  @Input() draggable: boolean
  lngLat: any;

  constructor() {
  }

  @ViewChild('#app') MyMap;

  Marker: any;
  ngOnInit(): void {
      this.MyMap = new Mapp({
          element: '#app',
          presets: {
              latlng: this.coordinates,
              zoom: 15
          },
          apiKey: environment.mapBoxKey
      });
      this.MyMap.addLayers();

      this.Marker = this.MyMap.addMarker({
        name: 'marker',
        latlng: this.coordinates,
        draggable: this.draggable,
      });

      this.Marker.on('dragend', this.onDragEnd);
    }

    onDragEnd = () => {
      this.lngLatEvent.emit(this.Marker._latlng)
    }

}
