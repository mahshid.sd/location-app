import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { AddressService } from 'src/app/Services/address.service';
import { AuthService } from '../../../Services/auth.service';

import { MatSnackBarModule } from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-address',
  templateUrl: './add-address.component.html',
  styleUrls: ['./add-address.component.scss']
})
export class AddAddressComponent implements OnInit {

  constructor(public addressService: AddressService, private router: Router, public authService: AuthService) { }

  ngOnInit(): void {
  }

  AddressForm = new FormGroup({
    userId: new FormControl(''),
    name: new FormControl(''),
    address: new FormControl(''),
    lng: new FormControl({lng:51.367918, lat:35.712706})
  });

  addLngLat(event: any) {
    this.AddressForm.value.lng = event
  }

  getUserid(){
    if(this.authService.logedIn()){
      this.AddressForm.value.userId = JSON.parse(localStorage.getItem('accessToken')).sub;
    }
  }

  onAddClick() {
    this.getUserid()
    this.addressService.addAddress(this.AddressForm.value).subscribe(
      () => {
        this.router.navigate([''])
      }
    );
  }

}
