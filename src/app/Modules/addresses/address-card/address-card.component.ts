import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

import { AddressService } from 'src/app/Services/address.service';
import { FavoriteAddressService } from 'src/app/Services/favorite-address.service';
import {AuthService} from 'src/app/Services/auth.service';


@Component({
  selector: 'app-address-card',
  templateUrl: './address-card.component.html',
  styleUrls: ['./address-card.component.scss']
})
export class AddressCardComponent implements OnInit{

  constructor(public addressService: AddressService, public favoritesService: FavoriteAddressService, public router: Router, public authService: AuthService, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  @Input() address: any
  @Input() FavoriteMood: boolean

  favoriteAddress = {};

  editMood: boolean = false;
  editedname = new FormControl('');
  editedaddress = new FormControl('');
  editedAddress = {}

  deleteAddress(id: number) {
    if(!this.FavoriteMood) {
      this.address = null;
      this.favoritesService.deteleFavoriteAddress(id).subscribe();
    }
    else {
      this.address = null;
      this.addressService.deleteAddress(id).subscribe();
      this.favoritesService.deteleFavoriteAddress(id).subscribe();
    }
  }

  onEditClick() {
    this.editMood = true;
    this.editedname = new FormControl(this.address.name);
    this.editedaddress = new FormControl(this.address.address);
  }

    editAddress(id: number) {
    this.addressService.getAddressDetails(id).subscribe(data =>{
      this.address = {
        "name": this.editedname.value,
        "address": this.editedaddress.value,
        "lng": data.lng
      }
      this.addressService.editAddress(id,this.address).subscribe();
      this.favoritesService.editFavoriteAddress(id, this.address).subscribe();
  })
      this.editMood = false;
  }

  onAddFavoriteClick() {
    if(this.authService.logedIn) {
      this.favoritesService.addFavoriteAddress(this.address).subscribe(
        () => this.openSnackBar('added Successfully :)', 'ok'),
        error => console.log(error)
      );

    } else {
      this.router.navigate(['favorite'])
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000
    });
  }
}
