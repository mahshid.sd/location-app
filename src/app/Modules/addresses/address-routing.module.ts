import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PublicAddressComponent } from './public-address/public-address.component';
import { FavoriteAddressComponent } from './favorite-address/favorite-address.component';
import { AddAddressComponent } from './add-address/add-address.component';
import { AuthGuard } from './Guard/auth.guard';


const routes: Routes = [
  {path: '', component: PublicAddressComponent},
  {path: 'favorite', component: FavoriteAddressComponent, canActivate: [AuthGuard]},
  {path: 'add', component: AddAddressComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class addressRoutingModule { }
