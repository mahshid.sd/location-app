import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../Services/auth.service';
import { UserService } from '../Services/user.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit, OnDestroy {
  user: any;
  title: string;

  ngOnInit(): void {
    this.getUser()
    this.logedIn()
  }

  mobileQuery: MediaQueryList;

  private _mobileQueryListener: () => void;

  constructor(private router: Router,
    private authService: AuthService,
    private userService: UserService,
    changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  onSignupClick() {
    this.router.navigate(['auth/signup']);
  }

  onLoginClick() {
    this.router.navigate(['auth/login']);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  logedIn() {
    return this.authService.logedIn();
  }

  onLogoutClick() {
    localStorage.removeItem('accessToken');
    this.router.navigate(['']);
    this.user = null;
  }

  getUser() {
      this.user = JSON.parse(localStorage.getItem('accessToken'))
      if(!this.logedIn()) {
        this.title = 'Weclome to my App'
      }
      else {
        this.title = this.user.email
      }
  }
}
