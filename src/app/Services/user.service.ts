import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  API_BASE_URL = environment.API_BASE_URL

  user: any;

  getUser(id: number){
    return this.http.get(`${this.API_BASE_URL}/users/${id}`)
  }
}
