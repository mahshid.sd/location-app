import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  constructor(private http: HttpClient) { }

  API_BASE_URL = environment.API_BASE_URL

  getAddress():Observable<any> {
    return this.http.get<any>(`${this.API_BASE_URL}/public-addresses`);
  }

  addAddress(address: any) {
    return this.http.post(`${this.API_BASE_URL}/public-addresses`, address);
  }

  deleteAddress(id: number) {
    return this.http.delete(`${this.API_BASE_URL}/public-addresses/${id}`)
  }

  editAddress(id: number, address: any) {
    return this.http.put(`${this.API_BASE_URL}/public-addresses/${id}`, address)
  }

  getAddressDetails(id: number){
    return this.http.get<any>(`${this.API_BASE_URL}/public-addresses/${id}`);
  }
}
