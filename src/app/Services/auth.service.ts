import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  API_BASE_URL = environment.API_BASE_URL

  SignUp(user: any) {
    return this.http.post(`${this.API_BASE_URL}/register`, user)
  }

  login(user: any) {
    return this.http.post(`${this.API_BASE_URL}/login`, user)
  }

  logedIn() {
    return !!localStorage.getItem("accessToken")
  }

}
