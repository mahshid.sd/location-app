import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FavoriteAddressService {

  constructor(private http: HttpClient) { }

  API_BASE_URL = environment.API_BASE_URL

  addFavoriteAddress(address) {
    return this.http.post(`${this.API_BASE_URL}/favorite-addresses`,address)
  }

  getFavoriteAddress(userId: number): Observable<any> {
    return this.http.get<any>(`${this.API_BASE_URL}/favorite-addresses?userId=${userId}`)
  }

  deteleFavoriteAddress(id: number) {
    return this.http.delete(`${this.API_BASE_URL}/favorite-addresses/${id}`)
  }

  editFavoriteAddress(id: number, address: any) {
    return this.http.put(`${this.API_BASE_URL}/favorite-addresses/${id}`, address)
  }
}
